#!/usr/bin/env node

thepath = '/home/ubuntu/'

process.title = 'tty.js';

var tty = require('tty.js');

var conf = tty.config.readConfig()
  , app = tty.createServer(conf);

app.listen();

app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var url = require("url")
  , redis = require('redis')
  , redis_validate = redis.createClient(6379, "192.168.100.150");

app.get('/getTreeData', function(req, res, next) {
  var parsedUrl = url.parse(req.url, true);
  var queryAsObject = parsedUrl.query;
  var token = queryAsObject._validation_token_key
  if (!token) {
    var result = [];
    res.json(result);
  }

  redis_validate.hget(["mySessionStore", token], function (err, session) {
    if (err || !session) {
      var result = [];
      res.json(result);
    } else {
      var result = dirTree(thepath).children;
      result = result.filter(function(el){
        return el.title[0] != ".";
      });
      res.json(result);
    }
  });
});

app.get('/getFileContent', function(req, res) {
  var parsedUrl = url.parse(req.url, true);
  var queryAsObject = parsedUrl.query;
  var token = queryAsObject._validation_token_key
  if (!token) {
    var result = {'success':false,'data':null};
    res.json(result);
  }

  redis_validate.hget(["mySessionStore", token], function (err, session) {
    if (err || !session) {
      var result = {'success':false,'data':null};
      res.json(result);
    } else {
      var filepath = queryAsObject.path
      filepath = thepath+"/"+filepath;
      var allFiles = getAllFiles(thepath);
      if (allFiles[filepath]) {
        var result = {'success':true};
        result['data'] = allFiles[filepath];
        res.json(result);
      }
      else {
        var result = {'success':false,'data':null};
        res.json(result);
      }
    }
  });
});


var fs = require('fs');
var path = require('path')

var save = app.io
  .of("/save")
  .on('connection', function(socket){
    socket.on('data', function(data){
      fs.writeFile(thepath+data.filename, data.value);
    });
  });

var open = app.io
  .of("/open")
  .on('connection', function(socket){
    socket.on('data', function(filepath){
      fs.readFile(thepath+filepath, function (err, data) {
        if (err) throw err;
        else {
          socket.emit('data', {'data':data.toString(),'path':filepath,'extension':path.extname(filepath),'filename':path.basename(filepath)});
        }
      });
    });
  });

var downloadSocket = app.io
  .of("/downloadSocket")
  .on('connection', function(socket){
    socket.on('reqfile', function(filename){
      fs.readFile(thepath+filename, function(err, data){
        if(err) throw err;
        else socket.emit('resfile', data.toString());
      });
    });

    socket.on('rename', function(filenames){
      fs.rename(thepath+filenames.oldname, thepath+filenames.newname, function (err) {
        if (err) throw err;
      });
    });

    socket.on('delfile', function(filename){
      fs.unlink(thepath+filename, function (err) {
        if (err) throw err;
      });
    });

    socket.on('createfolder', function(path){
      fs.mkdir(thepath+path, function(err) {
        if (err) throw err;
      });
    });

    socket.on('delfolder', function(foldername){
      fs.rmdir(thepath+foldername, function (err) {
        if (err) {
          if (err.code == 'ENOTEMPTY') {
            socket.emit('foldernotempty');
          }
          else {
            throw err;
          }
        }
      })
    });
  });

var path = require('path')

function dirTree(filename) {
  var stats = fs.lstatSync(filename),
    info = {
      title: path.basename(filename),
      path: filename.substring(thepath.length)
    };

  if (info.title[0] == '.' || (!stats.isFile() && !stats.isDirectory())) {
    info.exclude = true;
  }

  if (stats.isDirectory()) {
    info.folder = true;
    info.children = fs.readdirSync(filename).map(function(child) {
      return dirTree(filename + '/' + child);
    });
  }
  return info;
}

function getAllFiles(dir) {

  var results = {};

  fs.readdirSync(dir).forEach(function(file) {
    var info = {};
    file = dir+'/'+file;
    
    var stat = fs.statSync(file);

    if (stat && stat.isDirectory()) {
      results = MergeRecursive(results, getAllFiles(file));
    } 
    else {
      info.content = fs.readFileSync(file).toString();
      results[file] = info;
    }
  });
  return results;
};

function MergeRecursive(obj1, obj2) {

  for (var p in obj2) {
    try {
      // Property in destination object set; update its value.
      if ( obj2[p].constructor==Object ) {
        obj1[p] = MergeRecursive(obj1[p], obj2[p]);
      } else {
        obj1[p] = obj2[p];
      }
    } catch(e) {
      // Property in destination object not set; create it and set its value.
      obj1[p] = obj2[p];
    }
  }
  return obj1;
}
